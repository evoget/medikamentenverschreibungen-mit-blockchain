package ch.bfh.voget5.medical_prescription.client;

import java.beans.PropertyChangeEvent;
import java.util.List;

import org.eclipse.scout.rt.client.ui.action.keystroke.IKeyStroke;
import org.eclipse.scout.rt.client.ui.action.menu.AbstractMenu;
import org.eclipse.scout.rt.client.ui.action.menu.IMenu;
import org.eclipse.scout.rt.client.ui.desktop.AbstractDesktop;
import org.eclipse.scout.rt.client.ui.desktop.outline.AbstractOutlineViewButton;
import org.eclipse.scout.rt.client.ui.desktop.outline.IOutline;
import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.platform.text.TEXTS;
import org.eclipse.scout.rt.platform.util.CollectionUtility;

import ch.bfh.voget5.medical_prescription.client.Desktop.ThemeMenu.DarkThemeMenu;
import ch.bfh.voget5.medical_prescription.client.Desktop.ThemeMenu.DefaultThemeMenu;
import ch.bfh.voget5.medical_prescription.client.doctor.DoctorOutline;
import ch.bfh.voget5.medical_prescription.client.goverment.GovermentOutline;
import ch.bfh.voget5.medical_prescription.client.pharmacy.PharmacyOutline;
import ch.bfh.voget5.medical_prescription.shared.Icons;

/**
 * @author voget5
 */
public class Desktop extends AbstractDesktop {

	public Desktop() {
		addPropertyChangeListener(PROP_THEME, this::onThemeChanged);
	}

	@Override
	protected String getConfiguredTitle() {
		return TEXTS.get("ApplicationTitle");
	}

	@Override
	protected List<Class<? extends IOutline>> getConfiguredOutlines() {
		return CollectionUtility.<Class<? extends IOutline>>arrayList(DoctorOutline.class, PharmacyOutline.class,
				GovermentOutline.class);
	}

	@Override
	protected void execDefaultView() {
		selectFirstVisibleOutline();
	}

	protected void selectFirstVisibleOutline() {
		for (IOutline outline : getAvailableOutlines()) {
			if (outline.isEnabled() && outline.isVisible()) {
				setOutline(outline.getClass());
				return;
			}
		}
	}

	protected void onThemeChanged(PropertyChangeEvent evt) {
		IMenu darkMenu = getMenuByClass(DarkThemeMenu.class);
		IMenu defaultMenu = getMenuByClass(DefaultThemeMenu.class);
		String newThemeName = (String) evt.getNewValue();
		if (DarkThemeMenu.DARK_THEME.equalsIgnoreCase(newThemeName)) {
			darkMenu.setIconId(Icons.CheckedBold);
			defaultMenu.setIconId(null);
		} else {
			darkMenu.setIconId(null);
			defaultMenu.setIconId(Icons.CheckedBold);
		}
	}

	@Order(2000)
	public class ThemeMenu extends AbstractMenu {

		@Override
		protected String getConfiguredText() {
			return TEXTS.get("Theme");
		}

		@Order(1000)
		public class DefaultThemeMenu extends AbstractMenu {

			private static final String DEFAULT_THEME = "Default";

			@Override
			protected String getConfiguredText() {
				return DEFAULT_THEME;
			}

			@Override
			protected void execAction() {
				setTheme(DEFAULT_THEME.toLowerCase());
			}
		}

		@Order(2000)
		public class DarkThemeMenu extends AbstractMenu {

			private static final String DARK_THEME = "Dark";

			@Override
			protected String getConfiguredText() {
				return DARK_THEME;
			}

			@Override
			protected void execAction() {
				setTheme(DARK_THEME.toLowerCase());
			}
		}
	}

	@Order(1001)
	public class DoctorOutlineViewButton extends AbstractOutlineViewButton {

		public DoctorOutlineViewButton() {
			this(DoctorOutline.class);
		}

		protected DoctorOutlineViewButton(Class<? extends DoctorOutline> outlineClass) {
			super(Desktop.this, outlineClass);
		}

		@Override
		protected String getConfiguredKeyStroke() {
			return IKeyStroke.F2;
		}

		@Override
		protected DisplayStyle getConfiguredDisplayStyle() {
			return DisplayStyle.MENU;
		}
	}

	@Order(1002)
	public class PharmacyOutlineViewButton extends AbstractOutlineViewButton {

		public PharmacyOutlineViewButton() {
			this(PharmacyOutline.class);
		}

		protected PharmacyOutlineViewButton(Class<? extends PharmacyOutline> outlineClass) {
			super(Desktop.this, outlineClass);
		}

		@Override
		protected String getConfiguredKeyStroke() {
			return IKeyStroke.F3;
		}

		@Override
		protected DisplayStyle getConfiguredDisplayStyle() {
			return DisplayStyle.MENU;
		}
	}

	@Order(1003)
	public class WebserviceOutlineViewButton extends AbstractOutlineViewButton {

		public WebserviceOutlineViewButton() {
			this(GovermentOutline.class);
		}

		protected WebserviceOutlineViewButton(Class<? extends GovermentOutline> outlineClass) {
			super(Desktop.this, outlineClass);
		}

		@Override
		protected String getConfiguredKeyStroke() {
			return IKeyStroke.F4;
		}

		@Override
		protected DisplayStyle getConfiguredDisplayStyle() {
			return DisplayStyle.MENU;
		}
	}
}
