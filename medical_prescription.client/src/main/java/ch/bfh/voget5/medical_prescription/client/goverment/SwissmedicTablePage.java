package ch.bfh.voget5.medical_prescription.client.goverment;

import java.util.Date;
import java.util.Set;

import org.eclipse.scout.rt.client.dto.Data;
import org.eclipse.scout.rt.client.ui.action.menu.AbstractMenu;
import org.eclipse.scout.rt.client.ui.action.menu.IMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.TableMenuType;
import org.eclipse.scout.rt.client.ui.basic.table.AbstractTable;
import org.eclipse.scout.rt.client.ui.basic.table.ITableRow;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractDateColumn;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractStringColumn;
import org.eclipse.scout.rt.client.ui.desktop.outline.pages.AbstractPageWithTable;
import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.platform.text.TEXTS;
import org.eclipse.scout.rt.platform.util.CollectionUtility;
import org.eclipse.scout.rt.shared.services.common.jdbc.SearchFilter;

import ch.bfh.voget5.medical_prescription.client.goverment.SwissmedicTablePage.Table;
import ch.bfh.voget5.medical_prescription.shared.goverment.ISwissmedicService;
import voget5.medical_prescription.shared.webservice.SwissmedicTablePageData;

@Data(SwissmedicTablePageData.class)
public class SwissmedicTablePage extends AbstractPageWithTable<Table> {

	@Override
	protected String getConfiguredTitle() {
		return TEXTS.get("Swissmedic");
	}

	@Override
	protected void execLoadData(SearchFilter filter) {
		importPageData(BEANS.get(ISwissmedicService.class).getSwissmedicTableData(filter));
	}

	public class Table extends AbstractTable {

		public DosePowerNumberColumn getMyColumn() {
			return getColumnSet().getColumnByClass(DosePowerNumberColumn.class);
		}

		public ApprovalOwnerColumn getApprovalOwnerColumn() {
			return getColumnSet().getColumnByClass(ApprovalOwnerColumn.class);
		}

		public ApprovalDateColumn getApprovalDateColumn() {
			return getColumnSet().getColumnByClass(ApprovalDateColumn.class);
		}

		public ApprovalDateDosePowerColumn getApprovalDateDosePowerColumn() {
			return getColumnSet().getColumnByClass(ApprovalDateDosePowerColumn.class);
		}

		public ApprovalEndColumn getApprovalEndColumn() {
			return getColumnSet().getColumnByClass(ApprovalEndColumn.class);
		}

		public CategoryDosePowerColumn getCategoryDosePowerColumn() {
			return getColumnSet().getColumnByClass(CategoryDosePowerColumn.class);
		}

		public CategoryColumn getategoryColumn() {
			return getColumnSet().getColumnByClass(CategoryColumn.class);
		}

		public ActiveIngredientColumn getActiveIngredientColumn() {
			return getColumnSet().getColumnByClass(ActiveIngredientColumn.class);
		}

		public CompositionColumn getCompositionColumn() {
			return getColumnSet().getColumnByClass(CompositionColumn.class);
		}

		public DrugScopeColumn getDrugScopeColumn() {
			return getColumnSet().getColumnByClass(DrugScopeColumn.class);
		}

		public DrugCodeColumn getDrugCodeColumn() {
			return getColumnSet().getColumnByClass(DrugCodeColumn.class);
		}

		public DescriptionColumn getDescriptionColumn() {
			return getColumnSet().getColumnByClass(DescriptionColumn.class);
		}

		public ApprovalNumberColumn getApprovalNumberColumn() {
			return getColumnSet().getColumnByClass(ApprovalNumberColumn.class);
		}

		@Order(1000)
		public class ApprovalNumberColumn extends AbstractStringColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("approvalNumber");
			}

			@Override
			protected int getConfiguredWidth() {
				return 140;
			}
		}

		@Order(2000)
		public class DosePowerNumberColumn extends AbstractStringColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("dosePowerNumber");
			}

			@Override
			protected int getConfiguredWidth() {
				return 150;
			}
		}

		@Order(1500)
		public class DescriptionColumn extends AbstractStringColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("description");
			}

			@Override
			protected int getConfiguredWidth() {
				return 250;
			}
		}

		@Order(1600)
		public class ApprovalOwnerColumn extends AbstractStringColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("approvalOwner");
			}

			@Override
			protected int getConfiguredWidth() {
				return 250;
			}
		}

		@Order(1750)
		public class DrugCodeColumn extends AbstractStringColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("drugCode");
			}

			@Override
			protected int getConfiguredWidth() {
				return 150;
			}
		}

		@Order(6000)
		public class ApprovalDateColumn extends AbstractDateColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("approvalDate");
			}

			@Override
			protected int getConfiguredWidth() {
				return 125;
			}
		}

		@Order(7000)
		public class ApprovalDateDosePowerColumn extends AbstractDateColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("approvalDateDosePower");
			}

			@Override
			protected int getConfiguredWidth() {
				return 150;
			}
		}

		@Order(6500)
		public class ApprovalEndColumn extends AbstractDateColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("approvalEnd");
			}

			@Override
			protected int getConfiguredWidth() {
				return 125;
			}
		}

		@Order(9000)
		public class CategoryDosePowerColumn extends AbstractStringColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("categoryDosePower");
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}
		}

		@Order(1700)
		public class CategoryColumn extends AbstractStringColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("category");
			}

			@Override
			protected int getConfiguredWidth() {
				return 125;
			}
		}

		@Order(11000)
		public class ActiveIngredientColumn extends AbstractStringColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("activeIngredient");
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}
		}

		@Order(12000)
		public class CompositionColumn extends AbstractStringColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("composition");
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}
		}

		@Order(1775)
		public class DrugScopeColumn extends AbstractStringColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("drugScope");
			}

			@Override
			protected int getConfiguredWidth() {
				return 250;
			}
		}

		@Order(1000)
		public class DisableMenu extends AbstractMenu {
			@Override
			protected String getConfiguredText() {
				return TEXTS.get("invalid");
			}

			@Override
			protected Set<? extends IMenuType> getConfiguredMenuTypes() {
				return CollectionUtility.hashSet(TableMenuType.SingleSelection, TableMenuType.MultiSelection);
			}

			@Override
			protected void execAction() {
				for (ITableRow row : getSelectedRows()) {
					BEANS.get(ISwissmedicService.class).setApprovalEndDate(String.valueOf(row.getCellValue(0)),
							new Date(System.currentTimeMillis() - 24 * 60 * 60 * 1000)); // Yesterday
				}
				reloadPage();
			}
		}

		@Order(900)
		public class EnableMenu extends AbstractMenu {
			@Override
			protected String getConfiguredText() {
				return TEXTS.get("valid");
			}

			@Override
			protected Set<? extends IMenuType> getConfiguredMenuTypes() {
				return CollectionUtility.hashSet(TableMenuType.SingleSelection, TableMenuType.MultiSelection);
			}

			@Override
			protected void execAction() {
				for (ITableRow row : getSelectedRows()) {
					BEANS.get(ISwissmedicService.class).setApprovalEndDate(String.valueOf(row.getCellValue(0)),
							new Date(System.currentTimeMillis() + 24 * 60 * 60 * 1000)); // Tomorrow
				}
				reloadPage();
			}
		}
	}
}
