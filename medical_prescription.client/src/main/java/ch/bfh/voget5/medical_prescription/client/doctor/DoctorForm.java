package ch.bfh.voget5.medical_prescription.client.doctor;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.bouncycastle.jcajce.provider.digest.SHA3;
import org.bouncycastle.util.encoders.Hex;
import org.eclipse.scout.rt.client.dto.FormData;
import org.eclipse.scout.rt.client.ui.form.AbstractForm;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractButton;
import org.eclipse.scout.rt.client.ui.form.fields.datefield.AbstractDateField;
import org.eclipse.scout.rt.client.ui.form.fields.groupbox.AbstractGroupBox;
import org.eclipse.scout.rt.client.ui.form.fields.labelfield.AbstractLabelField;
import org.eclipse.scout.rt.client.ui.form.fields.smartfield.AbstractSmartField;
import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.platform.text.TEXTS;
import org.eclipse.scout.rt.platform.util.ObjectUtility;
import org.eclipse.scout.rt.shared.services.common.code.ICodeType;
import org.eclipse.scout.rt.shared.services.lookup.ILookupCall;

import ch.bfh.voget5.medical_prescription.client.doctor.DoctorForm.MainBox.ButtomBox.PrescriptionLabelField;
import ch.bfh.voget5.medical_prescription.client.doctor.DoctorForm.MainBox.ButtomBox.StorePrescriptionButton;
import ch.bfh.voget5.medical_prescription.client.doctor.DoctorForm.MainBox.TopBox;
import ch.bfh.voget5.medical_prescription.client.doctor.DoctorForm.MainBox.TopBox.DoctorField;
import ch.bfh.voget5.medical_prescription.client.doctor.DoctorForm.MainBox.TopBox.DrugField;
import ch.bfh.voget5.medical_prescription.client.doctor.DoctorForm.MainBox.TopBox.PatientField;
import ch.bfh.voget5.medical_prescription.client.doctor.DoctorForm.MainBox.TopBox.PrescriptionDateField;
import ch.bfh.voget5.medical_prescription.shared.PatientCodeType;
import ch.bfh.voget5.medical_prescription.shared.blockchain.IBlockchainService;
import ch.bfh.voget5.medical_prescription.shared.blockchain.PrescriptionStatus;
import ch.bfh.voget5.medical_prescription.shared.doctor.DoctorLookupCall;
import ch.bfh.voget5.medical_prescription.shared.swissmedic.DrugLookupCall;
import voget5.medical_prescription.shared.doctor.DoctorFormData;

@FormData(value = DoctorFormData.class, sdkCommand = FormData.SdkCommand.CREATE)
public class DoctorForm extends AbstractForm {

	@Override
	protected String getConfiguredTitle() {
		return TEXTS.get("Doctor");
	}

	@Override
	protected boolean getConfiguredSaveNeededVisible() {
		return false;
	}

	@Override
	protected boolean getConfiguredClosable() {
		return false;
	}

	public MainBox getMainBox() {
		return getFieldByClass(MainBox.class);
	}

	public TopBox getTopBox() {
		return getFieldByClass(TopBox.class);
	}

	public DoctorField getDoctorField() {
		return getFieldByClass(DoctorField.class);
	}

	public DrugField getDrugField() {
		return getFieldByClass(DrugField.class);
	}

	public PrescriptionDateField getPrescriptionDateField() {
		return getFieldByClass(PrescriptionDateField.class);
	}

	public PatientField getPatientField() {
		return getFieldByClass(PatientField.class);
	}

	public StorePrescriptionButton getstorePrescriptionButton() {
		return getFieldByClass(StorePrescriptionButton.class);
	}

	public PrescriptionLabelField getprescriptionLabelField() {
		return getFieldByClass(PrescriptionLabelField.class);
	}

	@Order(1000)
	public class MainBox extends AbstractGroupBox {

		@Override
		public int getGridColumnCount() {
			return 2;
		}

		@Order(1000)
		public class TopBox extends AbstractGroupBox {

			@Override
			public int getGridColumnCount() {
				return 1;
			}

			@Order(1000)
			public class DoctorField extends AbstractSmartField<String> {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Doctor");
				}

				@Override
				protected Class<? extends ILookupCall<String>> getConfiguredLookupCall() {
					return DoctorLookupCall.class;
				}

				@Override
				protected boolean getConfiguredMandatory() {
					return true;
				}

				@Override
				protected void execChangedValue() {
					getprescriptionLabelField().setVisible(false);
					super.execChangedValue();
				}
			}

			@Order(1500)
			public class PatientField extends AbstractSmartField<String> {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Patient");
				}

				@Override
				protected Class<? extends ICodeType<?, String>> getConfiguredCodeType() {
					return PatientCodeType.class;
				}

				@Override
				protected boolean getConfiguredMandatory() {
					return true;
				}

				@Override
				protected void execChangedValue() {
					getprescriptionLabelField().setVisible(false);
					super.execChangedValue();
				}
			}

			@Order(2000)
			public class DrugField extends AbstractSmartField<String> {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Drug");
				}

				@Override
				protected Class<? extends ILookupCall<String>> getConfiguredLookupCall() {
					return DrugLookupCall.class;
				}

				@Override
				protected boolean getConfiguredMandatory() {
					return true;
				}

				@Override
				protected void execChangedValue() {
					getprescriptionLabelField().setVisible(false);
					super.execChangedValue();
				}
			}

			@Order(2500)
			public class PrescriptionDateField extends AbstractDateField {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("GltigAb");
				}

				@Override
				protected void execInitField() {
					super.execInitField();
					setValue(new Date());
				}

				@Override
				protected boolean getConfiguredMandatory() {
					return true;
				}

				@Override
				protected void execChangedValue() {
					getprescriptionLabelField().setVisible(false);
					super.execChangedValue();
				}
			}

		}

		@Order(2000)
		public class ButtomBox extends AbstractGroupBox {
			String RED = "ffaf96";
			String GREEN = "aaffa6";

			@Order(3000)
			public class StorePrescriptionButton extends AbstractButton {

				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("storePrescription");
				}

				@Override
				protected void execClickAction() {
					String doctor = getDoctorField().getValue();
					String patient = getPatientField().getValue();
					String drug = getDrugField().getValue();
					String prescriptionDate = new SimpleDateFormat("dd.MM.yyyy")
							.format(getPrescriptionDateField().getValue());

					if (ObjectUtility.isOneOf(null, doctor, patient, drug, prescriptionDate)) {
						getprescriptionLabelField().setBackgroundColor(RED);
						getprescriptionLabelField().setValue("Bitte alle Felder ausfüllen.");
						return;
					}

					String prescriptionId = UUID.randomUUID().toString().substring(24);

					SHA3.DigestSHA3 digestSHA3 = new SHA3.Digest512();
					byte[] digest = digestSHA3
							.digest((doctor + patient + drug + prescriptionDate + prescriptionId).getBytes());
					PrescriptionStatus prescriptionStatus = BEANS.get(IBlockchainService.class)
							.storePrescription(Hex.toHexString(digest), doctor, drug);
					if (prescriptionStatus != null && prescriptionStatus.getResult()) {
						getprescriptionLabelField().setBackgroundColor(GREEN);
						getprescriptionLabelField().setValue("Rezept " + prescriptionId + " gespeichert.");
					} else {
						getprescriptionLabelField().setBackgroundColor(RED);
						getprescriptionLabelField().setValue("Rezept konnte nicht gespeichert werden. "
								+ (prescriptionStatus != null ? prescriptionStatus.getReason() : ""));
					}
					getprescriptionLabelField().setVisible(true);
				}
			}

			@Order(4000)
			@FormData(sdkCommand = FormData.SdkCommand.IGNORE)
			public class PrescriptionLabelField extends AbstractLabelField {
				@Override
				protected boolean getConfiguredLabelVisible() {
					return false;
				}

				@Override
				protected String getConfiguredFont() {
					return "BOLD";
				}
			}
		}
	}
}
