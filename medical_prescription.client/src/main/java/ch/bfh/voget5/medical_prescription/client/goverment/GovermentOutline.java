package ch.bfh.voget5.medical_prescription.client.goverment;

import java.util.List;

import org.eclipse.scout.rt.client.ui.desktop.outline.AbstractOutline;
import org.eclipse.scout.rt.client.ui.desktop.outline.pages.IPage;
import org.eclipse.scout.rt.platform.text.TEXTS;

import ch.bfh.voget5.medical_prescription.shared.Icons;

public class GovermentOutline extends AbstractOutline {
	@Override
	protected void execCreateChildPages(List<IPage<?>> pageList) {
		super.execCreateChildPages(pageList);
		pageList.add(new SwissmedicTablePage());
		pageList.add(new MedRegTablePage());
	}

	@Override
	protected String getConfiguredTitle() {
		return TEXTS.get("Goverment");
	}

	@Override
	protected String getConfiguredIconId() {
		return Icons.Folder;
	}
}
