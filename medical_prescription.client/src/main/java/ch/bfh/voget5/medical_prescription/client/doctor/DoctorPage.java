package ch.bfh.voget5.medical_prescription.client.doctor;

import org.eclipse.scout.rt.client.ui.desktop.outline.pages.AbstractPageWithNodes;
import org.eclipse.scout.rt.client.ui.form.IForm;
import org.eclipse.scout.rt.platform.text.TEXTS;


public class DoctorPage extends AbstractPageWithNodes {
	@Override
	protected boolean getConfiguredLeaf() {
		return true;
	}

	@Override
	protected boolean getConfiguredTableVisible() {
		return false;
	}

	@Override
	protected String getConfiguredTitle() {
		return TEXTS.get("Doctor");
	}

	@Override
	protected Class<? extends IForm> getConfiguredDetailForm() {
		return DoctorForm.class;
	}
}
