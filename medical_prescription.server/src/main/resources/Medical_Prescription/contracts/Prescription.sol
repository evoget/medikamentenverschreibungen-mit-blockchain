pragma solidity >=0.4.21 <0.7.0;

contract Prescription{
  string private prescriptionHash;
  bool private validity;
  uint private creationDate;
  mapping(string => bool) private doctorMapping;
  mapping(string => bool) private drugMapping;

  function storePrescriptionHash(string memory _prescriptionHash, string memory _gln, string memory _approvalNumber) public returns (bool){
    if ((keccak256(abi.encodePacked((prescriptionHash))) == keccak256(abi.encodePacked((_prescriptionHash))))){
      return false;
    }
    if (checkDoctor(_gln) && checkDrug(_approvalNumber)){
      prescriptionHash = _prescriptionHash;
      validity = true;
      creationDate = now;
      return true;
    } else {
      return false;
    }
  }

  function checkPrescriptionHash(string memory hashToCheck) public view returns (bool)  {
    return validity && ((creationDate + 2592000) > now) && (keccak256(abi.encodePacked((prescriptionHash))) == keccak256(abi.encodePacked((hashToCheck))));
  }

  function redeemPrescription(string memory _prescriptionHash, string memory _gln, string memory _approvalNumber) public returns (bool)  {
    if (validity
      && ((creationDate + 2592000) > now)
      && (keccak256(abi.encodePacked((prescriptionHash))) == keccak256(abi.encodePacked((_prescriptionHash))))
      && checkDoctor(_gln)
      && checkDrug(_approvalNumber)){
      validity = false;
      return true;
    }
    return false;
  }

  function addDoctor(string memory _gln) public {
      doctorMapping[_gln] = true;
  }

  function addDrug(string memory _approvalNumber) public {
      drugMapping[_approvalNumber] = true;
  }

  function deleteDoctor(string memory _gln) public{
      doctorMapping[_gln] = false;
  }

  function deleteDrug(string memory _approvalNumber) public{
      drugMapping[_approvalNumber] = false;
  }

  function checkDoctor(string memory _gln) public view returns (bool) {
    return doctorMapping[_gln];
  }
  function checkDrug(string memory _approvalNumber) public view returns (bool) {
    return drugMapping[_approvalNumber];
  }
}
