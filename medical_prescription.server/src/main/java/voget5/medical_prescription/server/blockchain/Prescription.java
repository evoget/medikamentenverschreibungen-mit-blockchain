package voget5.medical_prescription.server.blockchain;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Bool;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.RemoteFunctionCall;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import org.web3j.tx.gas.ContractGasProvider;

/**
 * <p>Auto generated code.
 * <p><strong>Do not modify!</strong>
 * <p>Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>,
 * or the org.web3j.codegen.SolidityFunctionWrapperGenerator in the 
 * <a href="https://github.com/web3j/web3j/tree/master/codegen">codegen module</a> to update.
 *
 * <p>Generated with web3j version 4.5.11.
 */
@SuppressWarnings("rawtypes")
public class Prescription extends Contract {
    public static final String BINARY = "608060405234801561001057600080fd5b50610e99806100206000396000f3fe608060405234801561001057600080fd5b50600436106100935760003560e01c80635e5ad6ec116100665780635e5ad6ec146104485780636a4e2b6a146105f6578063932f3fe01461069a578063b223f24f1461073e578063f26a7de4146107e257610093565b80630be6170014610098578063223883de1461013e578063239be0ba146101e25780635dbe2d4b146103a4575b600080fd5b61013c600480360360208110156100ae57600080fd5b810190602081018135600160201b8111156100c857600080fd5b8201836020820111156100da57600080fd5b803590602001918460018302840111600160201b831117156100fb57600080fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600092019190915250929550610886945050505050565b005b61013c6004803603602081101561015457600080fd5b810190602081018135600160201b81111561016e57600080fd5b82018360208201111561018057600080fd5b803590602001918460018302840111600160201b831117156101a157600080fd5b91908080601f0160208091040260200160405190810160405280939291908181526020018383808284376000920191909152509295506108fa945050505050565b610390600480360360608110156101f857600080fd5b810190602081018135600160201b81111561021257600080fd5b82018360208201111561022457600080fd5b803590602001918460018302840111600160201b8311171561024557600080fd5b91908080601f0160208091040260200160405190810160405280939291908181526020018383808284376000920191909152509295949360208101935035915050600160201b81111561029757600080fd5b8201836020820111156102a957600080fd5b803590602001918460018302840111600160201b831117156102ca57600080fd5b91908080601f0160208091040260200160405190810160405280939291908181526020018383808284376000920191909152509295949360208101935035915050600160201b81111561031c57600080fd5b82018360208201111561032e57600080fd5b803590602001918460018302840111600160201b8311171561034f57600080fd5b91908080601f01602080910402602001604051908101604052809392919081815260200183838082843760009201919091525092955061092d945050505050565b604080519115158252519081900360200190f35b610390600480360360208110156103ba57600080fd5b810190602081018135600160201b8111156103d457600080fd5b8201836020820111156103e657600080fd5b803590602001918460018302840111600160201b8311171561040757600080fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600092019190915250929550610a79945050505050565b6103906004803603606081101561045e57600080fd5b810190602081018135600160201b81111561047857600080fd5b82018360208201111561048a57600080fd5b803590602001918460018302840111600160201b831117156104ab57600080fd5b91908080601f0160208091040260200160405190810160405280939291908181526020018383808284376000920191909152509295949360208101935035915050600160201b8111156104fd57600080fd5b82018360208201111561050f57600080fd5b803590602001918460018302840111600160201b8311171561053057600080fd5b91908080601f0160208091040260200160405190810160405280939291908181526020018383808284376000920191909152509295949360208101935035915050600160201b81111561058257600080fd5b82018360208201111561059457600080fd5b803590602001918460018302840111600160201b831117156105b557600080fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600092019190915250929550610ae4945050505050565b61013c6004803603602081101561060c57600080fd5b810190602081018135600160201b81111561062657600080fd5b82018360208201111561063857600080fd5b803590602001918460018302840111600160201b8311171561065957600080fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600092019190915250929550610c26945050505050565b610390600480360360208110156106b057600080fd5b810190602081018135600160201b8111156106ca57600080fd5b8201836020820111156106dc57600080fd5b803590602001918460018302840111600160201b831117156106fd57600080fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600092019190915250929550610c59945050505050565b6103906004803603602081101561075457600080fd5b810190602081018135600160201b81111561076e57600080fd5b82018360208201111561078057600080fd5b803590602001918460018302840111600160201b831117156107a157600080fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600092019190915250929550610c8c945050505050565b61013c600480360360208110156107f857600080fd5b810190602081018135600160201b81111561081257600080fd5b82018360208201111561082457600080fd5b803590602001918460018302840111600160201b8311171561084557600080fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600092019190915250929550610d9f945050505050565b60016003826040518082805190602001908083835b602083106108ba5780518252601f19909201916020918201910161089b565b51815160209384036101000a60001901801990921691161790529201948552506040519384900301909220805460ff191693151593909317909255505050565b6001600482604051808280519060200190808383602083106108ba5780518252601f19909201916020918201910161089b565b6000836040516020018082805190602001908083835b602083106109625780518252601f199092019160209182019101610943565b6001836020036101000a03801982511681845116808217855250505050505090500191505060405160208183030381529060405280519060200120600060405160200180828054600181600116156101000203166002900480156109fd5780601f106109db5761010080835404028352918201916109fd565b820191906000526020600020905b8154815290600101906020018083116109e9575b5050915050604051602081830303815290604052805190602001201415610a2657506000610a72565b610a2f83610c59565b8015610a3f5750610a3f82610a79565b15610a6e578351610a57906000906020870190610dd2565b50506001805460ff19168117815542600255610a72565b5060005b9392505050565b60006004826040518082805190602001908083835b60208310610aad5780518252601f199092019160209182019101610a8e565b51815160209384036101000a600019018019909216911617905292019485525060405193849003019092205460ff16949350505050565b60015460009060ff168015610aff57504260025462278d0001115b8015610bf15750836040516020018082805190602001908083835b60208310610b395780518252601f199092019160209182019101610b1a565b6001836020036101000a0380198251168184511680821785525050505050509050019150506040516020818303038152906040528051906020012060006040516020018082805460018160011615610100020316600290048015610bd45780601f10610bb2576101008083540402835291820191610bd4565b820191906000526020600020905b815481529060010190602001808311610bc0575b505091505060405160208183030381529060405280519060200120145b8015610c015750610c0183610c59565b8015610c115750610c1182610a79565b15610a6e57506001805460ff19168155610a72565b6000600482604051808280519060200190808383602083106108ba5780518252601f19909201916020918201910161089b565b600060038260405180828051906020019080838360208310610aad5780518252601f199092019160209182019101610a8e565b60015460009060ff168015610ca757504260025462278d0001115b8015610d995750816040516020018082805190602001908083835b60208310610ce15780518252601f199092019160209182019101610cc2565b6001836020036101000a0380198251168184511680821785525050505050509050019150506040516020818303038152906040528051906020012060006040516020018082805460018160011615610100020316600290048015610d7c5780601f10610d5a576101008083540402835291820191610d7c565b820191906000526020600020905b815481529060010190602001808311610d68575b505091505060405160208183030381529060405280519060200120145b92915050565b6000600382604051808280519060200190808383602083106108ba5780518252601f19909201916020918201910161089b565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f10610e1357805160ff1916838001178555610e40565b82800160010185558215610e40579182015b82811115610e40578251825591602001919060010190610e25565b50610e4c929150610e50565b5090565b610e6a91905b80821115610e4c5760008155600101610e56565b9056fea165627a7a72305820d872ce36ecd2654bbe21097fd9326fbb73ac0a1e20a01720eeffe06b61ea03590029";

    public static final String FUNC_ADDDOCTOR = "addDoctor";

    public static final String FUNC_ADDDRUG = "addDrug";

    public static final String FUNC_STOREPRESCRIPTIONHASH = "storePrescriptionHash";

    public static final String FUNC_CHECKDRUG = "checkDrug";

    public static final String FUNC_REDEEMPRESCRIPTION = "redeemPrescription";

    public static final String FUNC_DELETEDRUG = "deleteDrug";

    public static final String FUNC_CHECKDOCTOR = "checkDoctor";

    public static final String FUNC_CHECKPRESCRIPTIONHASH = "checkPrescriptionHash";

    public static final String FUNC_DELETEDOCTOR = "deleteDoctor";

    @Deprecated
    protected Prescription(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    protected Prescription(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, credentials, contractGasProvider);
    }

    @Deprecated
    protected Prescription(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    protected Prescription(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public RemoteFunctionCall<TransactionReceipt> addDoctor(String _gln) {
        final Function function = new Function(
                FUNC_ADDDOCTOR, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Utf8String(_gln)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<TransactionReceipt> addDrug(String _approvalNumber) {
        final Function function = new Function(
                FUNC_ADDDRUG, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Utf8String(_approvalNumber)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<TransactionReceipt> storePrescriptionHash(String _prescriptionHash, String _gln, String _approvalNumber) {
        final Function function = new Function(
                FUNC_STOREPRESCRIPTIONHASH, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Utf8String(_prescriptionHash), 
                new org.web3j.abi.datatypes.Utf8String(_gln), 
                new org.web3j.abi.datatypes.Utf8String(_approvalNumber)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<Boolean> checkDrug(String _approvalNumber) {
        final Function function = new Function(FUNC_CHECKDRUG, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Utf8String(_approvalNumber)), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Bool>() {}));
        return executeRemoteCallSingleValueReturn(function, Boolean.class);
    }

    public RemoteFunctionCall<TransactionReceipt> redeemPrescription(String _prescriptionHash, String _gln, String _approvalNumber) {
        final Function function = new Function(
                FUNC_REDEEMPRESCRIPTION, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Utf8String(_prescriptionHash), 
                new org.web3j.abi.datatypes.Utf8String(_gln), 
                new org.web3j.abi.datatypes.Utf8String(_approvalNumber)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<TransactionReceipt> deleteDrug(String _approvalNumber) {
        final Function function = new Function(
                FUNC_DELETEDRUG, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Utf8String(_approvalNumber)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteFunctionCall<Boolean> checkDoctor(String _gln) {
        final Function function = new Function(FUNC_CHECKDOCTOR, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Utf8String(_gln)), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Bool>() {}));
        return executeRemoteCallSingleValueReturn(function, Boolean.class);
    }

    public RemoteFunctionCall<Boolean> checkPrescriptionHash(String hashToCheck) {
        final Function function = new Function(FUNC_CHECKPRESCRIPTIONHASH, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Utf8String(hashToCheck)), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Bool>() {}));
        return executeRemoteCallSingleValueReturn(function, Boolean.class);
    }

    public RemoteFunctionCall<TransactionReceipt> deleteDoctor(String _gln) {
        final Function function = new Function(
                FUNC_DELETEDOCTOR, 
                Arrays.<Type>asList(new org.web3j.abi.datatypes.Utf8String(_gln)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    @Deprecated
    public static Prescription load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new Prescription(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    @Deprecated
    public static Prescription load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new Prescription(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public static Prescription load(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        return new Prescription(contractAddress, web3j, credentials, contractGasProvider);
    }

    public static Prescription load(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        return new Prescription(contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public static RemoteCall<Prescription> deploy(Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        return deployRemoteCall(Prescription.class, web3j, credentials, contractGasProvider, BINARY, "");
    }

    @Deprecated
    public static RemoteCall<Prescription> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(Prescription.class, web3j, credentials, gasPrice, gasLimit, BINARY, "");
    }

    public static RemoteCall<Prescription> deploy(Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        return deployRemoteCall(Prescription.class, web3j, transactionManager, contractGasProvider, BINARY, "");
    }

    @Deprecated
    public static RemoteCall<Prescription> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(Prescription.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, "");
    }
}
