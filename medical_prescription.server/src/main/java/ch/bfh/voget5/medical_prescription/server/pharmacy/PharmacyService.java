package ch.bfh.voget5.medical_prescription.server.pharmacy;

import org.eclipse.scout.rt.platform.exception.VetoException;
import org.eclipse.scout.rt.platform.text.TEXTS;
import org.eclipse.scout.rt.shared.services.common.security.ACCESS;

import ch.bfh.voget5.medical_prescription.shared.pharmacy.CreatePharmacyPermission;
import ch.bfh.voget5.medical_prescription.shared.pharmacy.IPharmacyService;
import ch.bfh.voget5.medical_prescription.shared.pharmacy.ReadPharmacyPermission;
import ch.bfh.voget5.medical_prescription.shared.pharmacy.UpdatePharmacyPermission;
import voget5.medical_prescription.shared.pharmacy.PharmacyFormData;

public class PharmacyService implements IPharmacyService {

	@Override
	public PharmacyFormData prepareCreate(PharmacyFormData formData) {
		if (!ACCESS.check(new CreatePharmacyPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}
		// TODO [voget5] add business logic here.
		return formData;
	}

	@Override
	public PharmacyFormData create(PharmacyFormData formData) {
		if (!ACCESS.check(new CreatePharmacyPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}
		// TODO [voget5] add business logic here.
		return formData;
	}

	@Override
	public PharmacyFormData load(PharmacyFormData formData) {
		if (!ACCESS.check(new ReadPharmacyPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}
		// TODO [voget5] add business logic here.
		return formData;
	}

	@Override
	public PharmacyFormData store(PharmacyFormData formData) {
		if (!ACCESS.check(new UpdatePharmacyPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}
		// TODO [voget5] add business logic here.
		return formData;
	}
}
