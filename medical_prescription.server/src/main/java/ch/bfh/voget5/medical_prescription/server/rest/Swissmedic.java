package ch.bfh.voget5.medical_prescription.server.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import org.eclipse.scout.rt.platform.BEANS;

import ch.bfh.voget5.medical_prescription.server.swissmedic.SwissmedicService;

@Path("swissmedic")
public class Swissmedic {

	@GET
	@Produces({ MediaType.TEXT_PLAIN })
	@Path("{approvalNumber : \\d+}")
	public String checkApprovalNumber(@PathParam("approvalNumber") Long approvalNumber, @Context UriInfo u) {
		return String.valueOf(BEANS.get(SwissmedicService.class).checkApprovalNumber(String.valueOf(approvalNumber)));
	}
}
