package ch.bfh.voget5.medical_prescription.server.swissmedic;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.util.StringUtility;
import org.eclipse.scout.rt.shared.services.common.jdbc.SearchFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.bfh.voget5.medical_prescription.server.blockchain.BlockchainService;
import ch.bfh.voget5.medical_prescription.shared.goverment.ISwissmedicService;
import ch.bfh.voget5.medical_prescription.shared.swissmedic.Drug;
import voget5.medical_prescription.shared.webservice.SwissmedicTablePageData;
import voget5.medical_prescription.shared.webservice.SwissmedicTablePageData.SwissmedicTableRowData;

public class SwissmedicService implements ISwissmedicService {
	static Logger log = LoggerFactory.getLogger(SwissmedicService.class);
	private static final Set<Drug> drugSet = initialLoad();

	@Override
	public Set<Drug> getDrugSet() {
		return drugSet;
	}

	private static Set<Drug> initialLoad() {
		log.info("Start initialize drugs...");
		Set<Drug> newDrugStore = new HashSet<Drug>();
		try {
			List<String> allLines = Files
					.readAllLines(Paths.get(SwissmedicService.class.getResource("/Drugs/drugs.csv").toURI()));
			for (String line : allLines) {
				String[] drugEntry = line.split(";");
				if (drugEntry.length > 12) {
					Drug drug = new Drug();
					drug.setApprovalNumber(drugEntry[0]);
					drug.setDosePowerNumber(drugEntry[1]);
					drug.setDescription(drugEntry[2]);
					drug.setApprovalOwner(drugEntry[3]);
					drug.setDrugCode(drugEntry[4]);
					drug.setApprovalDate(drugEntry[5]);
					drug.setApprovalDateDosePower(drugEntry[6]);
					drug.setApprovalEnd(drugEntry[7]);
					drug.setCategoryDosePower(drugEntry[8]);
					drug.setCategory(drugEntry[9]);
					drug.setActiveIngredient(drugEntry[10]);
					drug.setComposition(drugEntry[11]);
					drug.setDrugScope(drugEntry[12]);
					newDrugStore.add(drug);
				}
			}
		} catch (IOException | URISyntaxException e) {
			log.error(e.getMessage(), e);
		}
		log.info(newDrugStore.size() + " drugs initialized");
		return newDrugStore;
	}

	@Override
	public boolean checkApprovalNumber(String approvalNumber) {
		for (Drug drug : drugSet) {
			if (StringUtility.equalsIgnoreCase(drug.getApprovalNumber(), approvalNumber)
					&& (drug.getApprovalEnd() == null || drug.getApprovalEnd().after(new Date()))) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void setApprovalEndDate(String approvalNumber, Date endDate) {
		Set<Drug> modifiedDrugSet = new HashSet<Drug>(drugSet.size());
		for (Drug drug : drugSet) {
			if (StringUtility.equalsIgnoreCase(drug.getApprovalNumber(), approvalNumber)) {
				drug.setApprovalEnd(endDate);
			}
			modifiedDrugSet.add(drug);
		}
		drugSet.clear();
		drugSet.addAll(modifiedDrugSet);

		if (endDate.after(new Date())) {
			BEANS.get(BlockchainService.class).addDrug(approvalNumber);
		} else {
			BEANS.get(BlockchainService.class).removeDrug(approvalNumber);
		}
	}

	@Override
	public SwissmedicTablePageData getSwissmedicTableData(SearchFilter filter) {
		SwissmedicTablePageData pageData = new SwissmedicTablePageData();
		Set<Drug> drugStore = BEANS.get(SwissmedicService.class).getDrugSet();
		for (Drug drug : drugStore) {
			SwissmedicTableRowData row = pageData.addRow();
			row.setApprovalNumber(drug.getApprovalNumber());
			row.setDescription(drug.getDescription());
			row.setApprovalOwner(drug.getApprovalOwner());
			row.setDosePowerNumber(drug.getDosePowerNumber());
			row.setDrugCode(drug.getDrugCode());
			row.setApprovalDate(drug.getApprovalDate());
			row.setApprovalDateDosePower(drug.getApprovalDateDosePower());
			row.setApprovalEnd(drug.getApprovalEnd());
			row.setCategoryDosePower(drug.getCategoryDosePower());
			row.setCategory(drug.getCategory());
			row.setActiveIngredient(drug.getActiveIngredient());
			row.setComposition(drug.getComposition());
			row.setDrugScope(drug.getDrugScope());
		}
		return pageData;
	}
}
