package ch.bfh.voget5.medical_prescription.server.blockchain;

import java.io.IOException;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.eclipse.scout.rt.platform.ApplicationScoped;
import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.text.TEXTS;
import org.eclipse.scout.rt.platform.util.ObjectUtility;
import org.eclipse.scout.rt.platform.util.StringUtility;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.gas.StaticGasProvider;

import ch.bfh.voget5.medical_prescription.server.medreg.MedregService;
import ch.bfh.voget5.medical_prescription.server.swissmedic.SwissmedicService;
import ch.bfh.voget5.medical_prescription.shared.blockchain.IBlockchainService;
import ch.bfh.voget5.medical_prescription.shared.blockchain.PrescriptionStatus;
import ch.bfh.voget5.medical_prescription.shared.medreg.Doctor;
import ch.bfh.voget5.medical_prescription.shared.swissmedic.Drug;

@ApplicationScoped
public class BlockchainService implements IBlockchainService {
	private Web3j web3j;
	private Credentials credsPrescription;
	private Credentials credsMedreg;
	private Prescription contract;
	private static final Logger LOG = LoggerFactory.getLogger(BlockchainService.class);

	public boolean initContractConnection() {
		if (ObjectUtility.isOneOf(null, web3j, credsPrescription, credsMedreg, contract)) {
			LOG.info("Connecting to blockchain ...");
			web3j = Web3j.build(new HttpService(String.format("http://%s:%s", "127.0.0.1", 7545)));
			credsPrescription = Credentials.create("a248478ad151ecd41e7dea39b2e2095110cfe8949a64ea226a1be55cc915673b");
			try {
				contract = Prescription.load(getContractAddress("Prescription"), web3j, credsPrescription,
						new StaticGasProvider(BigInteger.valueOf(5000), BigInteger.valueOf(4_100_000L)));

				LOG.info("Connected");
				for (Doctor doctor : BEANS.get(MedregService.class).getDoctorSet()) {
					contract.addDoctor(doctor.getGln()).send();
				}
				LOG.info("Loaded doctors into contract");
				for (Drug drug : BEANS.get(SwissmedicService.class).getDrugSet()) {
					contract.addDrug(drug.getApprovalNumber()).send();
				}
				LOG.info("Loaded drugs into contract");
			} catch (Exception e) {
				LOG.error(e.getMessage());
				return false;
			}
		}
		return true;
	}

	private String getContractAddress(String contractName) throws IOException, URISyntaxException {
		List<String> allLines = Files.readAllLines(Paths.get(SwissmedicService.class
				.getResource("/Medical_Prescription/build/contracts/" + contractName + ".json").toURI()));
		String jsonString = StringUtility.join("", allLines);
		JSONObject obj = new JSONObject(jsonString);
		return obj.getJSONObject("networks").getJSONObject("5777").getString("address");
	}

	private String getReasonToFail(String prescriptionHash, String gln, String approvalNumber, boolean redeemProcess)
			throws Exception {
		StringBuilder reasonToFail = new StringBuilder();
		Boolean prescriptionValid = contract.checkPrescriptionHash(prescriptionHash).send();
		Boolean doctorValid = contract.checkDoctor(gln).send();
		Boolean drugValid = contract.checkDrug(approvalNumber).send();

		if (redeemProcess && !prescriptionValid) {
			reasonToFail.append(TEXTS.get("prescriptionNotValid"));
		}
		if (!doctorValid) {
			reasonToFail.append(TEXTS.get("doctorNotAllowd"));
		}
		if (!drugValid) {
			reasonToFail.append(TEXTS.get("drugNotAllowed"));
		}
		return reasonToFail.toString();
	}

	@Override
	public PrescriptionStatus storePrescription(String prescriptionHash, String gln, String approvalNumber) {
		PrescriptionStatus prescriptionStatus = new PrescriptionStatus();
		try {
			TransactionReceipt receipt = contract.storePrescriptionHash(prescriptionHash, gln, approvalNumber).send();
			prescriptionStatus.setResult(contract.checkPrescriptionHash(prescriptionHash).send());
			LOG.info("Trying to store prescription in transaction " + receipt.getTransactionHash());
			if (prescriptionStatus.getResult()) {
				LOG.info("Prescription stored successfully");
				prescriptionStatus.setResult(true);
			} else {
				prescriptionStatus.setResult(false);
				prescriptionStatus.setReason(getReasonToFail(prescriptionHash, gln, approvalNumber, false));
				LOG.info("Prescription could not be stored. " + prescriptionStatus.getReason());
			}

		} catch (Exception e) {
			e.printStackTrace();
			prescriptionStatus.setReason("Error connection to blockchain");
			prescriptionStatus.setResult(false);

		}
		return prescriptionStatus;
	}

	@Override
	public PrescriptionStatus redeemPrescription(String prescriptionHash, String gln, String approvalNumber) {
		PrescriptionStatus prescriptionStatus = new PrescriptionStatus();
		prescriptionStatus.setResult(false);
		try {
			if (contract.checkPrescriptionHash(prescriptionHash).send()) {
				contract.redeemPrescription(prescriptionHash, gln, approvalNumber).send();
				LOG.info("Redeemed prescription");
				prescriptionStatus.setResult(!contract.checkPrescriptionHash(prescriptionHash).send());
			}
			prescriptionStatus.setReason(getReasonToFail(prescriptionHash, gln, approvalNumber, true));
		} catch (Exception e) {
			prescriptionStatus.setReason("Error connection to blockchain");
		}
		return prescriptionStatus;
	}

	@Override
	public boolean debug() {
		System.out.println("<<<<<<<<<<<<<<<< debug startet >>>>>>>>>>>>>>>>>");
		if (!initContractConnection()) {
			return false;
		}
		try {
			addDoctor("1");
			System.out.println("check doctor 1: " + contract.checkDoctor("2").send());
			System.out.println("check doctor 2: " + contract.checkDoctor("2").send());

			addDoctor("2");
			System.out.println("check doctor 1: " + contract.checkDoctor("2").send());
			System.out.println("check doctor 2: " + contract.checkDoctor("2").send());

			contract.deleteDoctor("1").send();
			System.out.println("check doctor 1: " + contract.checkDoctor("2").send());
			System.out.println("check doctor 2: " + contract.checkDoctor("2").send());
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public void addDoctor(String gln) {
		try {
			contract.addDoctor(gln).send();
			LOG.info("Added doctor with GLN=" + gln);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void removeDoctor(String gln) {
		try {
			contract.deleteDoctor(gln).send();
			LOG.info("Removed doctor with GLN=" + gln);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void addDrug(String approvalNumber) {
		try {
			contract.addDrug(approvalNumber).send();
			LOG.info("Added drug with ApprovalNumber=" + approvalNumber);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void removeDrug(String approvalNumber) {
		try {
			contract.deleteDrug(approvalNumber).send();
			LOG.info("Removed drug with ApprovalNumber=" + approvalNumber);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
