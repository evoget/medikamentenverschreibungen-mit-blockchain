package ch.bfh.voget5.medical_prescription.server.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import org.eclipse.scout.rt.platform.BEANS;

import ch.bfh.voget5.medical_prescription.server.medreg.MedregService;

@Path("medreg")
public class Medreg {

	@GET
	@Produces({ MediaType.TEXT_PLAIN })
	@Path("{gln : \\d+}")
	/**
	 * 
	 * @param gln Global Location Number / Eindeutige Nummer einer Medizinalperson
	 */
	public String checkGln(@PathParam("gln") Long gln, @Context UriInfo u) {
		System.out.println(String.valueOf(gln) + "was checked.");
		return String.valueOf(BEANS.get(MedregService.class).checkApproval(String.valueOf(gln)));
	}
}
