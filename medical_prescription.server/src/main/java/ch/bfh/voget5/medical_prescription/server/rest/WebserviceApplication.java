package ch.bfh.voget5.medical_prescription.server.rest;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.moxy.json.MoxyJsonFeature;
import org.glassfish.jersey.moxy.xml.MoxyXmlFeature;
import org.glassfish.jersey.server.ResourceConfig;

public class WebserviceApplication extends ResourceConfig {
	private static Class<?>[] restClasses = { Medreg.class, Swissmedic.class,
			// Jackson Provider for content handling
			MoxyJsonFeature.class, MoxyXmlFeature.class };

	public WebserviceApplication() {
		super(restClasses);
		register(new LoggingFeature(Logger.getLogger(LoggingFeature.DEFAULT_LOGGER_NAME), Level.ALL,
				LoggingFeature.Verbosity.PAYLOAD_ANY, 300000));
	}
}
