package ch.bfh.voget5.medical_prescription.server.goverment;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.testing.platform.runner.RunWithSubject;
import org.eclipse.scout.rt.testing.server.runner.RunWithServerSession;
import org.eclipse.scout.rt.testing.server.runner.ServerTestRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

import ch.bfh.voget5.medical_prescription.server.ServerSession;
import ch.bfh.voget5.medical_prescription.server.swissmedic.SwissmedicService;

@RunWithSubject("anonymous")
@RunWith(ServerTestRunner.class)
@RunWithServerSession(ServerSession.class)
public class SwissmedicServiceTest {

	@Test
	public void testSwissmedic() {
		SwissmedicService swissmedicService = BEANS.get(SwissmedicService.class);
		assertNotNull("No drugs loaded", swissmedicService.getDrugSet());

		assertTrue(swissmedicService.checkApprovalNumber("57002"));
		swissmedicService.setApprovalEndDate("57002", new Date(System.currentTimeMillis() - 24 * 60 * 60 * 1000));
		assertFalse(swissmedicService.checkApprovalNumber("57002"));
		swissmedicService.setApprovalEndDate("57002", new Date(System.currentTimeMillis() + 24 * 60 * 60 * 1000));
		assertTrue(swissmedicService.checkApprovalNumber("57002"));
	}
}
