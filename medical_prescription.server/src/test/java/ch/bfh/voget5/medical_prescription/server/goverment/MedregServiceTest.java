package ch.bfh.voget5.medical_prescription.server.goverment;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.testing.platform.runner.RunWithSubject;
import org.eclipse.scout.rt.testing.server.runner.RunWithServerSession;
import org.eclipse.scout.rt.testing.server.runner.ServerTestRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

import ch.bfh.voget5.medical_prescription.server.ServerSession;
import ch.bfh.voget5.medical_prescription.server.medreg.MedregService;

@RunWithSubject("anonymous")
@RunWith(ServerTestRunner.class)
@RunWithServerSession(ServerSession.class)
public class MedregServiceTest {

	@Test
	public void testMedreg() {
		MedregService medregService = BEANS.get(MedregService.class);
		assertNotNull("No doctors loaded", medregService.getDoctorSet());
		assertTrue(medregService.checkApproval("5872154231325"));
		medregService.setApproval("5872154231325", false);
		assertFalse(medregService.checkApproval("5872154231325"));
		medregService.setApproval("5872154231325", true);
		assertTrue(medregService.checkApproval("5872154231325"));
	}
}
