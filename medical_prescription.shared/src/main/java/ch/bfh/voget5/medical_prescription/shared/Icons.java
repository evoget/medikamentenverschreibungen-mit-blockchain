package ch.bfh.voget5.medical_prescription.shared;

import org.eclipse.scout.rt.shared.AbstractIcons;

/**
 * @author voget5
 */
public class Icons extends AbstractIcons {

	private static final long serialVersionUID = 1L;

	public static final String Scout = "eclipse_scout";
	public static final String User = "user";
	public static final String AppLogo = "application_logo";
}
