package ch.bfh.voget5.medical_prescription.shared.swissmedic;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.scout.rt.platform.util.StringUtility;

public class Drug {
	private String approvalNumber; // Zulassungsnummer
	private String dosePowerNumber; // Dosisstärkenummer
	private String description; // Bezeichnung des Arzneimittels
	private String approvalOwner; // Zulassungsinhaberin
	private String drugCode; // Heilmittelcode
	private Date approvalDate; // Erstzulassungsdatum Arzneimittel
	private Date approvalDateDosePower; // Zul.datum Dosisstärke
	private Date approvalEnd; // Gültigkeitsdauer der Zulassung
	private String categoryDosePower; // Abgabekategorie Dosisstärke
	private String category; // Abgabekategorie Arzneimittel
	private String activeIngredient; // Wirkstoff(e)
	private String composition; // Zusammensetzung
	private String drugScope; // Anwendungsgebiet Arzneimittel

	public String getApprovalNumber() {
		return approvalNumber;
	}

	public void setApprovalNumber(String approvalNumber) {
		this.approvalNumber = approvalNumber;
	}

	public String getDosePowerNumber() {
		return dosePowerNumber;
	}

	public void setDosePowerNumber(String dosePowerNumber) {
		this.dosePowerNumber = dosePowerNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getApprovalOwner() {
		return approvalOwner;
	}

	public void setApprovalOwner(String approvalOwner) {
		this.approvalOwner = approvalOwner;
	}

	public String getDrugCode() {
		return drugCode;
	}

	public void setDrugCode(String drugCode) {
		this.drugCode = drugCode;
	}

	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(String approvalDateString) {
		try {
			Date approvalDate = new SimpleDateFormat("dd.MM.yyyy").parse(approvalDateString);
			this.approvalDate = approvalDate;

		} catch (ParseException e) {
			// No approvalDate for this drug
		}
	}

	public Date getApprovalDateDosePower() {
		return approvalDateDosePower;
	}

	public void setApprovalDateDosePower(String approvalDateDosePowerString) {
		try {
			Date approvalDateDosePower = new SimpleDateFormat("dd.MM.yyyy").parse(approvalDateDosePowerString);
			this.approvalDateDosePower = approvalDateDosePower;
		} catch (ParseException e) {
			// No approvalDateDosePower for this drug
		}
	}

	public Date getApprovalEnd() {
		return approvalEnd;
	}

	public void setApprovalEnd(String approvalEndString) {
		if (StringUtility.equalsIgnoreCase(approvalEndString, "unbegrenzt")) {
			return; // No approvalEnd for this drug
		}
		try {
			Date approvalEnd = new SimpleDateFormat("dd.MM.yyyy").parse(approvalEndString);
			this.approvalEnd = approvalEnd;
		} catch (ParseException e) {
			// No approvalEnd for this drug
		}

	}

	public void setApprovalEnd(Date approvalEnd) {
		this.approvalEnd = approvalEnd;
	}

	public String getCategoryDosePower() {
		return categoryDosePower;
	}

	public void setCategoryDosePower(String categoryDosePower) {
		this.categoryDosePower = categoryDosePower;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getActiveIngredient() {
		return activeIngredient;
	}

	public void setActiveIngredient(String activeIngredient) {
		this.activeIngredient = activeIngredient;
	}

	public String getComposition() {
		return composition;
	}

	public void setComposition(String composition) {
		this.composition = composition;
	}

	public String getDrugScope() {
		return drugScope;
	}

	public void setDrugScope(String drugScope) {
		this.drugScope = drugScope;
	}
}
