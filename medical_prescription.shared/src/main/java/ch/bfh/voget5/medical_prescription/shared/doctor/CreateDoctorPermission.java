package ch.bfh.voget5.medical_prescription.shared.doctor;

import java.security.BasicPermission;

public class CreateDoctorPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public CreateDoctorPermission() {
		super(CreateDoctorPermission.class.getSimpleName());
	}
}
