package ch.bfh.voget5.medical_prescription.shared.blockchain;

import java.io.Serializable;

public class PrescriptionStatus implements Serializable {
	private static final long serialVersionUID = 1L;
	String reason;
	Boolean result;

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Boolean getResult() {
		return result;
	}

	public void setResult(Boolean result) {
		this.result = result;
	}
}
