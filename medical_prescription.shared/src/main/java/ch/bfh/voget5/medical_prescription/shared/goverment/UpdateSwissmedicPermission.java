package ch.bfh.voget5.medical_prescription.shared.goverment;

import java.security.BasicPermission;

public class UpdateSwissmedicPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public UpdateSwissmedicPermission() {
		super(UpdateSwissmedicPermission.class.getSimpleName());
	}
}
