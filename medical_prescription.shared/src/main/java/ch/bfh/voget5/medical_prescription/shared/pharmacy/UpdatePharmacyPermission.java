package ch.bfh.voget5.medical_prescription.shared.pharmacy;

import java.security.BasicPermission;

public class UpdatePharmacyPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public UpdatePharmacyPermission() {
		super(UpdatePharmacyPermission.class.getSimpleName());
	}
}
