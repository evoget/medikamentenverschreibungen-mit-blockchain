package ch.bfh.voget5.medical_prescription.shared.goverment;

import java.util.Date;
import java.util.Set;

import org.eclipse.scout.rt.platform.service.IService;
import org.eclipse.scout.rt.shared.TunnelToServer;
import org.eclipse.scout.rt.shared.services.common.jdbc.SearchFilter;

import ch.bfh.voget5.medical_prescription.shared.swissmedic.Drug;
import voget5.medical_prescription.shared.webservice.SwissmedicTablePageData;

@TunnelToServer
public interface ISwissmedicService extends IService {

	SwissmedicTablePageData getSwissmedicTableData(SearchFilter filter);

	Set<Drug> getDrugSet();

	boolean checkApprovalNumber(String approvalNumber);

	void setApprovalEndDate(String approvalNumber, Date endDate);

}
