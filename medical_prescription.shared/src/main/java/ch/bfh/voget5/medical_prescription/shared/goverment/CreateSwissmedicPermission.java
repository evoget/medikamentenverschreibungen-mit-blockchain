package ch.bfh.voget5.medical_prescription.shared.goverment;

import java.security.BasicPermission;

public class CreateSwissmedicPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public CreateSwissmedicPermission() {
		super(CreateSwissmedicPermission.class.getSimpleName());
	}
}
