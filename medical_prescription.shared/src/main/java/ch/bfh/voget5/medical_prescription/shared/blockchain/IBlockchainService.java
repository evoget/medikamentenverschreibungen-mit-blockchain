package ch.bfh.voget5.medical_prescription.shared.blockchain;

import org.eclipse.scout.rt.platform.service.IService;
import org.eclipse.scout.rt.shared.TunnelToServer;

@TunnelToServer
public interface IBlockchainService extends IService {
	PrescriptionStatus storePrescription(String prescriptionHash, String gln, String approvalNumber);

	PrescriptionStatus redeemPrescription(String prescriptionHash, String gln, String approvalNumber);

	boolean debug();

	void addDoctor(String gln);

	void removeDoctor(String gln);

	void removeDrug(String approvalNumber);

	void addDrug(String approvalNumber);
}