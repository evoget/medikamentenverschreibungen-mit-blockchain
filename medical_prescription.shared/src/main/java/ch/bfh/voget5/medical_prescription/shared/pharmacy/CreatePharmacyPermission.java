package ch.bfh.voget5.medical_prescription.shared.pharmacy;

import java.security.BasicPermission;

public class CreatePharmacyPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public CreatePharmacyPermission() {
		super(CreatePharmacyPermission.class.getSimpleName());
	}
}
