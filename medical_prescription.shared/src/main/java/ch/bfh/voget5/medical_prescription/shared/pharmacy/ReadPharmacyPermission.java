package ch.bfh.voget5.medical_prescription.shared.pharmacy;

import java.security.BasicPermission;

public class ReadPharmacyPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public ReadPharmacyPermission() {
		super(ReadPharmacyPermission.class.getSimpleName());
	}
}
