package ch.bfh.voget5.medical_prescription.shared.pharmacy;

import org.eclipse.scout.rt.platform.service.IService;
import org.eclipse.scout.rt.shared.TunnelToServer;

import voget5.medical_prescription.shared.pharmacy.PharmacyFormData;

@TunnelToServer
public interface IPharmacyService extends IService {

	PharmacyFormData prepareCreate(PharmacyFormData formData);

	PharmacyFormData create(PharmacyFormData formData);

	PharmacyFormData load(PharmacyFormData formData);

	PharmacyFormData store(PharmacyFormData formData);
}
