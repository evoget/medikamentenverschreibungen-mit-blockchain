package ch.bfh.voget5.medical_prescription.shared.doctor;

import org.eclipse.scout.rt.shared.services.common.code.AbstractCodeType;

public class DoctorCodeType extends AbstractCodeType<Long, String> {

	private static final long serialVersionUID = 1L;
	public static final long ID = 0L;

	@Override
	public Long getId() {
		return ID;
	}
}
